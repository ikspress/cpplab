#include "../../include/app_utils.h"
#include "../../include/attributes.h"
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>

#if __has_include(<termios.h>)
    #include <termios.h>
#endif

#ifdef _WIN32
    #include <io.h>
    #ifdef NO_OLDNAMES
        #define isatty _isatty
    #endif
#else
    #include <unistd.h>
#endif

void safe_fgets(char *str, int count, FILE *stream) {
    int errno_save = errno, ferror_save = ferror(stream);
    while (fgets(str, count, stream) != nullptr && !feof(stream)) {
        if (errno == EINTR) {
            // Clear any errors we set and try again
            errno = errno_save;
            if (!ferror_save)
                clearerr(stream);
        } else {
            break;
        }
    }
}

CPPLAB_ATTRIB_format(printf, 2, 0) static bool question(bool preset, char const *format, va_list args) {
    fflush(stderr);
    fflush(stdout);
    int fd_out = fileno(stdout);
#ifdef TCIFLUSH
    int fd_in = fileno(stdin);
    if (isatty(fd_in))
        tcflush(fd_in, TCIFLUSH);
#elifdef _WIN32
#else
    fflush(stdin);
#endif
    vprintf(format, args);
    fwrite(preset ? " [Y/n] " : " [y/N] ", sizeof(char), 8, stdout);
    fflush(stdout);
    char resp[32];
    safe_fgets(resp, sizeof(resp), stdin);
}

bool yesno(char const *format, ...) {
    va_list args;
    va_start(args, format);
    bool ret = question(true, format, args);
    va_end(args);
    return ret;
}

bool noyes(char const *format, ...) {
    va_list args;
    va_start(args, format);
    bool ret = question(false, format, args);
    va_end(args);
    return ret;
}

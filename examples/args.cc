#include <cstdio>
#include <exception>
#include <fmt/base.h>

int main(int argc, char *argv[]) try {
    for (int i = 0; i < argc; ++i)
        fmt::println("argv[{}] = {}", i, argv[i]);
    return 0;
} catch (std::exception const &e) {
    fmt::println(stderr, "{}", e.what());
    return 1;
}

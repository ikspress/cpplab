#include <exception>
#include <iostream>
#include <string>

int main() try {
    std::string str;
    while (std::getline(std::cin, str))
        std::cout << str << '\n';
    return 0;
} catch (std::exception const &e) {
    std::cerr << e.what() << '\n';
    return 1;
}

#include <cstdint>
#include <exception>
#include <iostream>

int main() try {
    std::uint32_t height;
    std::cin.exceptions(std::istream::failbit);
    std::cout << "Enter height:\n";
    std::cin >> height;
    for (std::uint32_t i = 1; i <= height; ++i) {
        for (std::uint32_t j = i; j < height; ++j)
            std::cout.put(' ');
        for (std::uint32_t j = 1; j < i * 2; ++j)
            std::cout.put('*');
        std::cout.put('\n');
    }
    return 0;
} catch (std::exception const &e) {
    std::cerr << e.what() << '\n';
    return 1;
}

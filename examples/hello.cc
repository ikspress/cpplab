#include <cstdio>
#include <exception>
#include <fmt/base.h>

int main() try {
    fmt::println("Hello, World!");
    return 0;
} catch (std::exception const &e) {
    fmt::println(stderr, "{}", e.what());
    return 1;
}

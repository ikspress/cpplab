#include <cstdio>
#include <exception>
#include <fmt/base.h>
#include <memory>
#include <random>

int main() try {
    std::random_device rng;
    using ResultType = decltype(rng)::result_type;
    constexpr ResultType lower = 1u, upper = 100u;
    std::uniform_int_distribution<ResultType> uni(lower, upper);
    ResultType target = uni(rng), guess;
    fmt::println("Guess a number between {} and {}.", lower, upper);
    for (ResultType tries = 0;; ++tries) {
        std::scanf("%u", std::addressof(guess));
        if (guess > target) {
            fmt::println("Try lower.");
        } else if (guess < target) {
            fmt::println("Try higher.");
        } else {
            fmt::println("Correct! You got it in {} guesses.", tries);
            break;
        }
    }
    return 0;
} catch (std::exception const &e) {
    fmt::println(stderr, "{}", e.what());
    return 1;
}

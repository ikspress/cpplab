#include "../../include/app_utils.h"
#include "../../include/platform/win32/handle_guard.hh"
#include "../../include/platform/win32/winapi.hh"
#include <charconv>
#include <cstdio>
#include <cstdlib>
#include <exception>
#include <fmt/base.h>
#include <getopt.h>
#include <memory>
#include <ntstatus.h>
#include <windows.h>

#ifdef _MSC_VER
    #pragma comment(lib, "advapi32.lib")
    #pragma comment(lib, "ntdll.lib")
    #pragma comment(lib, "shell32.lib")
#endif

void usage() {
    fmt::print(USAGE_HEADER
               "{} <command>\n" USAGE_SEPARATOR
               "Triggers a Blue Screen of Death (BSoD) without drivers.\n" USAGE_SEPARATOR USAGE_COMMANDS
               "  -d, --dead-proc          set as a critical process and then self-terminate\n"
               "  -r, --raise-err[=status] send a hard error message with status code\n" USAGE_HELP_ARG("{:>31}"),
               PROGNAME, USAGE_HELP_MSG);
}

void enable_single_priv(LPCTSTR name) {
    cpplab::win32::handle_guard token = cpplab::win32::OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES);
    cpplab::win32::TOKEN_PRIVILEGES<1> priv;
    LookupPrivilegeValue(nullptr, name, std::addressof(priv.Privileges[0].Luid));
    priv.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
    cpplab::win32::AdjustTokenPrivileges(token, priv);
}

void raise_hard_error(NTSTATUS code) {
    using enum cpplab::win32::HARDERROR_RESPONSE_OPTION;
    cpplab::win32::HARDERROR_RESPONSE resp;
    cpplab::win32::NtRaiseHardError(code, 0, 0, nullptr, OptionShutdownSystem, std::addressof(resp));
}

void set_critical_state() {
#if true
    using enum cpplab::win32::PROCESSINFOCLASS;
    ULONG enable = 1;
    cpplab::win32::NtSetInformationProcess(GetCurrentProcess(), ProcessBreakOnTermination, std::addressof(enable),
                                           sizeof(enable));
#else
    cpplab::win32::RtlSetProcessIsCritical(TRUE, nullptr);
#endif
}

int main(int argc, char *argv[]) try {
    enum { ACT_NONE, ACT_SET_CRITIC, ACT_RAISE_ERROR } act = ACT_NONE;
    static option const longopts[] = {
        {"dead-proc",       no_argument, nullptr, 'd'},
        {"raise-err", optional_argument, nullptr, 'r'},
        HELP_OPTION,
        {    nullptr,                 0, nullptr,   0}
    };
    for (int opt; (opt = getopt_long(argc, argv, "dhr::", longopts, nullptr)) != -1;) {
        switch (opt) {
        case 'd':
            act = ACT_SET_CRITIC;
            break;
        case 'r':
            act = ACT_RAISE_ERROR;
            break;
        case 'h':
            usage();
            return EXIT_SUCCESS;
        case '?':
            fmt::println(USAGE_INVALID_ARG("{}"), PROGNAME);
            return EXIT_FAILURE;
        }
    }
    fmt::print("WARNING: It won't bring any benefits to you.\n"
               "Please note that all unsaved work will be lost.\n");
    if (!noyes("Do you want to continue?"))
        return EXIT_SUCCESS;
    switch (act) {
    case ACT_NONE:
        fmt::println("No action was selected.");
        return EXIT_FAILURE;
    case ACT_SET_CRITIC:
        enable_single_priv(SE_DEBUG_NAME);
        set_critical_state();
        break;
    case ACT_RAISE_ERROR:
        NTSTATUS code = STATUS_SUCCESS;
        if (optarg)
            std::from_chars(optarg, nullptr, code, 16);
        enable_single_priv(SE_SHUTDOWN_NAME);
        // ORed with 0xC0000000 to guarantee NT_ERROR(code) is true
        raise_hard_error(STATUS_SEVERITY_ERROR << 30 | code);
        break;
    }
    return EXIT_SUCCESS;
} catch (std::exception const &e) {
    fmt::println(stderr, "{}", e.what());
    return EXIT_FAILURE;
}

#ifndef CPPLAB_RAND48_HH
#define CPPLAB_RAND48_HH

#include <cstdint>
#include <random>

namespace cpplab {

using GlibcRand48 = std::linear_congruential_engine<std::uint_least64_t, 0x5DEECE66D, 0xB, 0x1000000000000>;

} // namespace cpplab

#endif

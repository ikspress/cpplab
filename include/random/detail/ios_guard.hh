#ifndef CPPLAB_IOS_GUARD_HH
#define CPPLAB_IOS_GUARD_HH

#include <ios>

namespace cpplab::detail {

template <typename CharType, typename Traits>
class FlagGuard {
    FlagGuard(FlagGuard const &) = delete;
    FlagGuard &operator=(FlagGuard const &) = delete;

public:
    using StreamType = std::basic_ios<CharType, Traits>;

    explicit FlagGuard(StreamType &stream) : m_stream(stream), m_fmtflags(stream.flags()) {}

    ~FlagGuard() {
        this->m_stream.flags(this->m_fmtflags);
    }

private:
    StreamType &m_stream;
    StreamType::fmtflags m_fmtflags;
};

template <typename CharType, typename Traits>
class FillGuard {
    FillGuard(FillGuard const &) = delete;
    FillGuard &operator=(FillGuard const &) = delete;

public:
    using StreamType = std::basic_ios<CharType, Traits>;

    explicit FillGuard(StreamType &stream) : m_stream(stream), m_fill(stream.fill()) {}

    ~FillGuard() {
        this->m_stream.fill(this->m_fill);
    }

private:
    StreamType &m_stream;
    StreamType::char_type m_fill;
};

} // namespace cpplab::detail

#endif

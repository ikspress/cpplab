#ifndef CPPLAB_XORSHIFT_HH
#define CPPLAB_XORSHIFT_HH

#include "detail/ios_guard.hh"
#include <concepts>
#include <cstdint>
#include <istream>
#include <limits>
#include <ostream>

namespace cpplab {

namespace detail {

template <std::unsigned_integral T, int a, int b, int c>
class XorshiftEngine {
public:
    using result_type = T;
    constexpr static result_type default_seed = 1;

    XorshiftEngine() = default;

    explicit XorshiftEngine(result_type value) noexcept {
        this->seed(value);
    }

    constexpr static result_type max() noexcept {
        return std::numeric_limits<result_type>::max();
    }

    constexpr static result_type min() noexcept {
        return std::numeric_limits<result_type>::min();
    }

    void discard(unsigned long long z) noexcept {
        for (; z != 0; --z)
            (*this)();
    }

    void seed(result_type value) noexcept {
        this->m_x = value;
    }

    result_type operator()() noexcept {
        this->m_x ^= this->m_x << a;
        this->m_x ^= this->m_x >> b;
        this->m_x ^= this->m_x << c;
        return this->m_x;
    }

    friend bool operator==(XorshiftEngine const &lhs, XorshiftEngine const &rhs) noexcept {
        return lhs.m_x == rhs.m_x;
    }

    template <typename CharType, typename Traits>
    friend std::basic_ostream<CharType, Traits> &operator<<(std::basic_ostream<CharType, Traits> &os,
                                                            XorshiftEngine const &rng) {
        FillGuard<CharType, Traits> guard1(os);
        FlagGuard<CharType, Traits> guard2(os);
        os.flags(std::basic_istream<CharType, Traits>::dec | std::basic_istream<CharType, Traits>::left);
        os.fill(os.widen(' '));
        return os << rng.m_x;
    }

    template <typename CharType, typename Traits>
    friend std::basic_istream<CharType, Traits> &operator>>(std::basic_istream<CharType, Traits> &is,
                                                            XorshiftEngine &rng) {
        FlagGuard<CharType, Traits> guard(is);
        is.flags(std::basic_istream<CharType, Traits>::dec);
        return is >> rng.m_x;
    }

private:
    result_type m_x = default_seed;
};

} // namespace detail

using Xorshift8 = detail::XorshiftEngine<std::uint8_t, 3, 1, 5>;
using Xorshift16 = detail::XorshiftEngine<std::uint16_t, 7, 9, 8>;
using Xorshift32 = detail::XorshiftEngine<std::uint32_t, 13, 17, 5>;
using Xorshift64 = detail::XorshiftEngine<std::uint64_t, 13, 7, 17>;

} // namespace cpplab

#endif

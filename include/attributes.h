#ifndef CPPLAB_ATTRIBUTES_HH
#define CPPLAB_ATTRIBUTES_HH

#ifdef __has_cpp_attribute
    #define CPPLAB_HAS_ATTRIBUTE __has_cpp_attribute
#elifdef __has_c_attribute
    #define CPPLAB_HAS_ATTRIBUTE __has_c_attribute
#else
    #define CPPLAB_HAS_ATTRIBUTE(...) 0
#endif

// forceinline
#if CPPLAB_HAS_ATTRIBUTE(clang::always_inline)
    #define CPPLAB_ATTRIB_forceinline [[clang::always_inline]]
#elif CPPLAB_HAS_ATTRIBUTE(gnu::always_inline)
    #define CPPLAB_ATTRIB_forceinline [[gnu::always_inline]]
#elif CPPLAB_HAS_ATTRIBUTE(msvc::forceinline)
    #define CPPLAB_ATTRIB_forceinline [[msvc::forceinline]]
#else
    #define CPPLAB_ATTRIB_forceinline
#endif

// forceinline for lambda expressions
#ifdef __GNUC__
    #define CPPLAB_ATTRIB_forceinline_lambda __attribute__((always_inline))
#elifdef _MSC_VER
    #define CPPLAB_ATTRIB_forceinline_lambda [[msvc::forceinline]]
#else
    #define CPPLAB_ATTRIB_forceinline_lambda
#endif

// format
#if CPPLAB_HAS_ATTRIBUTE(gnu::format)
    #define CPPLAB_ATTRIB_format(archetype, index, start) [[gnu::format(archetype, index, start)]]
#else
    #define CPPLAB_ATTRIB_format(...)
#endif

// may_alias
#if CPPLAB_HAS_ATTRIBUTE(gnu::may_alias)
    #define CPPLAB_ATTRIB_may_alias [[gnu::may_alias]]
#else
    #define CPPLAB_ATTRIB_may_alias
#endif

#endif

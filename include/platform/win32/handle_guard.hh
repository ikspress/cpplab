#ifndef CPPLAB_GUARD_HH
#define CPPLAB_GUARD_HH

#include <windows.h>

namespace cpplab::win32 {

class handle_guard {
    handle_guard(handle_guard const &) = delete;
    handle_guard &operator=(handle_guard const &) = delete;

public:
    handle_guard(HANDLE handle) : m_handle(handle) {}

    ~handle_guard() {
        CloseHandle(this->m_handle);
    }

    constexpr explicit operator bool() const noexcept {
        return !this->m_handle;
    }

    constexpr operator HANDLE() const noexcept {
        return this->m_handle;
    }

private:
    HANDLE m_handle;
};

} // namespace cpplab::win32

#endif

#ifndef CPPLAB_WINAPI_HH
#define CPPLAB_WINAPI_HH

#include "../../attributes.h"
#include <cstddef>
#include <memory>
#include <windows.h>

namespace cpplab::win32 {

extern "C" {

enum class HARDERROR_RESPONSE : ULONG {
    ResponseReturnToCaller,
    ResponseNotHandled,
    ResponseAbort,
    ResponseCancel,
    ResponseIgnore,
    ResponseNo,
    ResponseOk,
    ResponseRetry,
    ResponseYes,
    ResponseTryAgain,
    ResponseContinue
};

using PHARDERROR_RESPONSE = HARDERROR_RESPONSE *;

enum class HARDERROR_RESPONSE_OPTION : ULONG {
    OptionAbortRetryIgnore,
    OptionOk,
    OptionOkCancel,
    OptionRetryCancel,
    OptionYesNo,
    OptionYesNoCancel,
    OptionShutdownSystem,
    OptionOkNoWait,
    OptionCancelTryContinue
};

enum class PROCESSINFOCLASS {
    ProcessBasicInformation,
    ProcessQuotaLimits,
    ProcessIoCounters,
    ProcessVmCounters,
    ProcessTimes,
    ProcessBasePriority,
    ProcessRaisePriority,
    ProcessDebugPort,
    ProcessExceptionPort,
    ProcessAccessToken,
    ProcessLdtInformation,
    ProcessLdtSize,
    ProcessDefaultHardErrorMode,
    ProcessIoPortHandlers,
    ProcessPooledUsageAndLimits,
    ProcessWorkingSetWatch,
    ProcessUserModeIOPL,
    ProcessEnableAlignmentFaultFixup,
    ProcessPriorityClass,
    ProcessWx86Information,
    ProcessHandleCount,
    ProcessAffinityMask,
    ProcessPriorityBoost,
    ProcessDeviceMap,
    ProcessSessionInformation,
    ProcessForegroundInformation,
    ProcessWow64Information,
    ProcessImageFileName,
    ProcessLUIDDeviceMapsEnabled,
    ProcessBreakOnTermination,
    ProcessDebugObjectHandle,
    ProcessDebugFlags,
    ProcessHandleTracing,
    ProcessIoPriority,
    ProcessExecuteFlags,
    ProcessResourceManagement,
    ProcessCookie,
    ProcessImageInformation,
    MaxProcessInfoClass
};

enum class THREADINFOCLASS {
    ThreadBasicInformation,
    ThreadTimes,
    ThreadPriority,
    ThreadBasePriority,
    ThreadAffinityMask,
    ThreadImpersonationToken,
    ThreadDescriptorTableEntry,
    ThreadEnableAlignmentFaultFixup,
    ThreadEventPair_Reusable,
    ThreadQuerySetWin32StartAddress,
    ThreadZeroTlsCell,
    ThreadPerformanceCount,
    ThreadAmILastThread,
    ThreadIdealProcessor,
    ThreadPriorityBoost,
    ThreadSetTlsArrayAddress,
    ThreadIsIoPending,
    ThreadHideFromDebugger,
    ThreadBreakOnTermination,
    ThreadSwitchLegacyState,
    ThreadIsTerminated,
    MaxThreadInfoClass
};

NTSYSCALLAPI NTSTATUS NTAPI NtQueryInformationProcess(HANDLE ProcessHandle, PROCESSINFOCLASS ProcessInformationClass,
                                                      PVOID ProcessInformation, ULONG ProcessInformationLength,
                                                      PULONG ReturnLength) noexcept;

NTSYSCALLAPI NTSTATUS NTAPI NtQueryInformationThread(HANDLE ThreadHandle, THREADINFOCLASS ThreadInformationClass,
                                                     PVOID ThreadInformation, ULONG ThreadInformationLength,
                                                     PULONG ReturnLength) noexcept;

NTSYSCALLAPI NTSTATUS NTAPI NtRaiseHardError(NTSTATUS ErrorStatus, ULONG NumberOfParameters,
                                             ULONG UnicodeStringParameterMask, PULONG_PTR Parameters,
                                             HARDERROR_RESPONSE_OPTION ResponseOption,
                                             PHARDERROR_RESPONSE Response) noexcept;

NTSYSCALLAPI NTSTATUS NTAPI NtSetInformationProcess(HANDLE ProcessHandle, PROCESSINFOCLASS ProcessInformationClass,
                                                    PVOID ProcessInformation, ULONG ProcessInformationLength) noexcept;

NTSYSCALLAPI NTSTATUS NTAPI NtSetInformationThread(HANDLE ThreadHandle, THREADINFOCLASS ThreadInformationClass,
                                                   PVOID ThreadInformation, ULONG ThreadInformationLength) noexcept;

NTSYSAPI NTSTATUS NTAPI RtlAdjustPrivilege(ULONG Privilege, BOOLEAN Enable, BOOLEAN Client, PBOOLEAN WasEnabled);

NTSYSAPI NTSTATUS STDAPIVCALLTYPE RtlSetProcessIsCritical(BOOLEAN NewValue, PBOOLEAN OldValue,
                                                          BOOLEAN CheckFlag = FALSE) noexcept;

NTSYSAPI NTSTATUS STDAPIVCALLTYPE RtlSetThreadIsCritical(BOOLEAN NewValue, PBOOLEAN OldValue,
                                                         BOOLEAN CheckFlag = FALSE) noexcept;
}

template <std::size_t count>
struct CPPLAB_ATTRIB_may_alias TOKEN_PRIVILEGES {
    DWORD PrivilegeCount = count;
    LUID_AND_ATTRIBUTES Privileges[count];
};

template <std::size_t x>
CPPLAB_ATTRIB_forceinline DWORD AdjustTokenPrivileges(HANDLE token, TOKEN_PRIVILEGES<x> new_state) {
    DWORD length;
    ::AdjustTokenPrivileges(token, FALSE, reinterpret_cast<PTOKEN_PRIVILEGES>(std::addressof(new_state)), 0, nullptr,
                            std::addressof(length));
    return length;
}

template <std::size_t x, std::size_t y>
CPPLAB_ATTRIB_forceinline DWORD AdjustTokenPrivileges(HANDLE token, TOKEN_PRIVILEGES<x> new_state,
                                                      TOKEN_PRIVILEGES<y> &old_state) {
    DWORD length;
    ::AdjustTokenPrivileges(token, FALSE, reinterpret_cast<PTOKEN_PRIVILEGES>(std::addressof(new_state)),
                            sizeof(old_state), reinterpret_cast<PTOKEN_PRIVILEGES>(std::addressof(old_state)),
                            std::addressof(length));
    return length;
}

template <std::size_t x>
CPPLAB_ATTRIB_forceinline DWORD ClearTokenPrivileges(HANDLE token, TOKEN_PRIVILEGES<x> &old_state) {
    DWORD length;
    ::AdjustTokenPrivileges(token, TRUE, nullptr, sizeof(old_state),
                            reinterpret_cast<PTOKEN_PRIVILEGES>(std::addressof(old_state)), std::addressof(length));
    return length;
}

CPPLAB_ATTRIB_forceinline inline HANDLE OpenProcessToken(HANDLE process, DWORD accesses) {
    HANDLE token;
    ::OpenProcessToken(process, accesses, std::addressof(token));
    return token;
}

} // namespace cpplab::win32

#endif

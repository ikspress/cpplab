#ifndef CPPLAB_APP_UTILS_H
#define CPPLAB_APP_UTILS_H

#include "../include/attributes.h"

#define PROGNAME ""

// Constant strings for usage() functions
#define USAGE_ARGUMENTS        "Arguments:\n"
#define USAGE_COMMANDS         "Commands:\n"
#define USAGE_HEADER           "Usage: "
#define USAGE_HELP_ARG(fmt)    "  -h, --help " fmt "\n"
#define USAGE_HELP_MSG         "display this help"
#define USAGE_INVALID_ARG(fmt) "Try '" fmt " --help' for more information."
#define USAGE_MANPAGE(fmt)     "For more details see " fmt ".\n"
#define USAGE_OPTIONS          "Options:\n"
#define USAGE_SEPARATOR        "\n"
#define USAGE_VER_ARG(fmt)     "  -V, --version " fmt "\n"
#define USAGE_VER_MSG          "display version"

#define HELP_OPTION {"help", no_argument, nullptr, 'h'}
#define VER_OPTION  {"version", no_argument, nullptr, 'V'}

#ifdef __cplusplus
extern "C" {
#endif

CPPLAB_ATTRIB_format(printf, 1, 2) [[nodiscard]] bool yesno(char const *format, ...);
CPPLAB_ATTRIB_format(printf, 1, 2) [[nodiscard]] bool noyes(char const *format, ...);

#ifdef __cplusplus
}
#endif

#endif

#ifndef CPPLAB_ABS_HH
#define CPPLAB_ABS_HH

#include <concepts>
#include <type_traits>

namespace cpplab {

template <std::integral T>
    requires(!std::is_same_v<T, bool>)
[[nodiscard]] constexpr std::make_unsigned_t<T> absu(T n) {
    if constexpr (std::is_signed_v<T>)
        n = n < 0 ? -n : n;
    return n;
}

} // namespace cpplab

#endif

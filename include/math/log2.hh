#ifndef CPPLAB_LOG2_HH
#define CPPLAB_LOG2_HH

#include <bit>
#include <concepts>
#include <type_traits>

namespace cpplab {

template <std::integral T>
[[nodiscard]] constexpr int log2i(T n) noexcept {
    return std::bit_width<std::make_unsigned_t<T>>(n) - 1;
}

} // namespace cpplab

#endif

#ifndef CPPLAB_INTROSORT_HH
#define CPPLAB_INTROSORT_HH

#include "../../math/log2.hh"
#include "detail/sort_n.hh"
#include "detail/unguarded_insertion_sort.hh"
#include "heapsort.hh"
#include "insertion_sort.hh"
#include <algorithm>
#include <functional>
#include <iterator>

namespace cpplab {

namespace detail::introsort {

template <typename Iterator>
constexpr inline std::iter_difference_t<Iterator> introsort_threshold = 16;

template <typename Iterator, typename Compare>
constexpr Iterator unguarded_partition(Iterator first, Iterator last, Compare comp) {
    auto pivot = first;
    while (true) {
        while (comp(*++first, *pivot)) {}
        while (comp(*pivot, *--last)) {}
        if (first >= last) {
            std::iter_swap(--first, pivot);
            return first;
        }
        std::iter_swap(first, last);
    }
}

template <typename Iterator, typename Compare>
constexpr void introsort_loop(Iterator first, Iterator last, Compare comp, int depth_limit) {
    for (std::iter_difference_t<Iterator> length; (length = last - first) > introsort_threshold<Iterator>;) {
        if (depth_limit == 0) [[unlikely]] {
            // When the depth of recursion exceeds depth_limit, switches to
            // heapsort to ensure an O(n log n) complexity.
            heapsort(first, last, comp);
            return;
        }
        sort3(first + length / 2, first, last - 1, comp);
        Iterator i = unguarded_partition(first, last, comp);
        introsort_loop(i + 1, last, comp, --depth_limit);
        last = i;
    }
}

} // namespace detail::introsort

template <typename Iterator, typename Compare = std::less<>>
constexpr void introsort(Iterator first, Iterator last, Compare comp = {}) {
    std::iter_difference_t<Iterator> length = last - first;
    detail::introsort::introsort_loop(first, last, comp, log2i(length) * 2);
    if (length > detail::introsort::introsort_threshold<Iterator>) {
        Iterator i = first + detail::introsort::introsort_threshold<Iterator>;
        detail::unguarded_insertion_sort(i, last, comp);
        last = i;
    }
    insertion_sort(first, last, comp);
}

} // namespace cpplab

#endif

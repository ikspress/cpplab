#ifndef CPPLAB_HEAPSORT_HH
#define CPPLAB_HEAPSORT_HH

#include <algorithm>
#include <functional>

namespace cpplab {

template <typename Iterator, typename Compare = std::less<>>
constexpr void heapsort(Iterator first, Iterator last, Compare comp = {}) {
    std::make_heap(first, last, comp);
    std::sort_heap(first, last, comp);
}

} // namespace cpplab

#endif

#ifndef CPPLAB_BUBBLE_SORT_HH
#define CPPLAB_BUBBLE_SORT_HH

#include <algorithm>
#include <functional>
#include <iterator>

namespace cpplab {

template <typename Iterator, typename Compare = std::less<>>
constexpr void bubble_sort(Iterator first, Iterator last, Compare comp = {}) {
    for (bool swapped = true; first != last && swapped; --last) {
        swapped = false;
        for (auto i = std::next(first); i != last; ++i) {
            auto prev_i = std::prev(i);
            if (comp(*i, *prev_i)) {
                std::iter_swap(i, prev_i);
                swapped = true;
            }
        }
    }
}

} // namespace cpplab

#endif

#ifndef CPPLAB_PDQSORT_HH
#define CPPLAB_PDQSORT_HH

#include "../../attributes.h"
#include "../../math/log2.hh"
#include "detail/branchless.hh"
#include "detail/partial_insertion_sort.hh"
#include "detail/sort_n.hh"
#include "detail/unguarded_insertion_sort.hh"
#include "detail/unguarded_partition_equal.hh"
#include "heapsort.hh"
#include "insertion_sort.hh"
#include <algorithm>
#include <cstddef>
#include <functional>
#include <iterator>
#include <optional>
#include <utility>

namespace cpplab {

namespace detail::pdqsort {

constexpr inline std::ptrdiff_t insertion_sort_threshold = 24;
constexpr inline std::ptrdiff_t ninther_threshold = 128;

template <typename Iterator, typename Difference>
constexpr void break_patterns(Iterator first, Iterator pivot, Iterator last, Difference left_length,
                              Difference right_length) {
    if (left_length >= insertion_sort_threshold) {
        std::iter_swap(first, first + left_length / 4);
        std::iter_swap(pivot - 1, pivot - left_length / 4);
        if (left_length > ninther_threshold) {
            std::iter_swap(first + 1, first + (left_length / 4 + 1));
            std::iter_swap(first + 2, first + (left_length / 4 + 2));
            std::iter_swap(pivot - 2, pivot - (left_length / 4 + 1));
            std::iter_swap(pivot - 3, pivot - (left_length / 4 + 2));
        }
    }
    if (right_length >= insertion_sort_threshold) {
        std::iter_swap(pivot + 1, pivot + (right_length / 4 + 1));
        std::iter_swap(last - 1, last - right_length / 4);
        if (right_length > ninther_threshold) {
            std::iter_swap(pivot + 2, pivot + (right_length / 4 + 2));
            std::iter_swap(pivot + 3, pivot + (right_length / 4 + 3));
            std::iter_swap(last - 2, last - (right_length / 4 + 1));
            std::iter_swap(last - 3, last - (right_length / 4 + 2));
        }
    }
}

template <typename Iterator, typename Compare, typename Distance>
constexpr void choose_pivot(Iterator first, Iterator last, Compare comp, Distance length) {
    Iterator middle = first + length / 2;
    sort3(middle, first, last - 1, comp);
    if (length > ninther_threshold) {
        // Tukey's ninther
        // https://doi.org/10.1016/B978-0-12-204750-3.50024-1
        sort3(first + 1, middle - 1, last - 2, comp);
        sort3(first + 2, middle + 1, last - 3, comp);
        sort3(middle - 1, first, middle + 1, comp);
    }
}

template <typename Iterator, typename T, typename Compare>
constexpr Iterator do_partition_branched(Iterator first, Iterator last, T &pivot, Compare comp) {
    while (first < last) {
        std::iter_swap(first, last);
        while (comp(*++first, pivot)) {}
        while (comp(pivot, *--last)) {}
    }
    return first;
}

template <typename Iterator, typename T, typename Compare>
constexpr Iterator do_partition_branchless(Iterator first, Iterator last, T &pivot, Compare comp) {}

template <typename Iterator, typename Compare>
constexpr std::pair<Iterator, bool> unguarded_partition(Iterator first, Iterator last, Compare comp) {
    Iterator i = first;
    auto pivot = std::move(*i);
    // Exclude elements that are already in the correct position.
    while (comp(*++first, pivot)) {}
    while (comp(pivot, *--last)) {}
    bool no_swap = first >= last;
    while (first < last) {
        std::iter_swap(first, last);
        while (comp(*++first, pivot)) {}
        while (comp(pivot, *--last)) {}
    }
    *i = std::move(*--first);
    *first = std::move(pivot);
    return std::make_pair(first, no_swap);
}

template <typename Iterator, typename Compare>
constexpr void pdqsort_loop(Iterator first, Iterator last, Compare comp, int bad_allowed,
                            std::optional<Iterator> &&ancestor_pivot = {}) {
    for (std::iter_difference_t<Iterator> length; (length = last - first) >= insertion_sort_threshold;) {
        choose_pivot(first, last, comp, length);
        if (ancestor_pivot
                .transform([&comp, &first](auto &&x) CPPLAB_ATTRIB_forceinline_lambda { return !comp(*x, *first); })
                .value_or(false)) {
            first = unguarded_partition_equal(first, last, comp);
            continue;
        }
        auto [pivot, perfect] = unguarded_partition(first, last, comp);
        if (auto left_length = pivot - first, right_length = last - pivot, limit_length = length / 8;
            left_length < limit_length || right_length < limit_length) [[unlikely]] {
            if (--bad_allowed == 0) [[unlikely]] {
                heapsort(first, last, comp);
                return;
            }
            break_patterns(first, pivot, last, left_length, right_length);
        } else if (perfect) {
            // The handling of perfect partition is the same as
            // std::__introsort() in libc++.
            if (bool left_ordered = partial_insertion_sort(first, pivot, comp);
                partial_insertion_sort(pivot + 1, last, comp)) {
                if (left_ordered)
                    return;
                last = pivot;
                continue;
            } else if (left_ordered) {
                first = pivot + 1;
                ancestor_pivot = pivot;
                continue;
            }
        }
        pdqsort_loop(first, pivot, comp, bad_allowed, std::forward<decltype(ancestor_pivot)>(ancestor_pivot));
        first = pivot + 1;
        ancestor_pivot = pivot;
    }
    if (ancestor_pivot)
        unguarded_insertion_sort(first, last, comp);
    else
        insertion_sort(first, last, comp);
}

} // namespace detail::pdqsort

template <typename Iterator, typename Compare = std::less<>>
constexpr void pdqsort(Iterator first, Iterator last, Compare comp = {}) {
    detail::pdqsort::pdqsort_loop(first, last, comp, log2i(last - first));
}

} // namespace cpplab

#endif

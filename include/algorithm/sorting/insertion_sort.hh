#ifndef CPPLAB_INSERTION_SORT_HH
#define CPPLAB_INSERTION_SORT_HH

#include <algorithm>
#include <functional>
#include <iterator>
#include <utility>

namespace cpplab {

template <typename Iterator, typename Compare = std::less<>>
constexpr void insertion_sort(Iterator first, Iterator last, Compare comp = {}) {
    if (first == last)
        return;
    for (auto i = std::next(first); i != last;) {
        auto prev_i = std::prev(i), next_i = std::next(i);
        if (comp(*i, *prev_i)) {
            auto temp = std::move(*i);
            if (comp(*i, *first)) {
                std::move_backward(first, i, next_i);
                *first = std::move(temp);
            } else {
                auto j = i;
                do {
                    *j = std::move(*prev_i);
                    j = prev_i;
                } while (comp(temp, *--prev_i));
                *j = std::move(temp);
            }
        }
        i = next_i;
    }
}

} // namespace cpplab

#endif

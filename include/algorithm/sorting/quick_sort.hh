#ifndef CPPLAB_QUICK_SORT_HH
#define CPPLAB_QUICK_SORT_HH

#include "../../attributes.h"
#include <algorithm>
#include <functional>
#include <iterator>
#include <utility>

namespace cpplab {

namespace detail::qsort {

template <typename Iterator, typename T, typename Predicate>
constexpr Iterator do_partition(Iterator first, Iterator last, T &pivot, Predicate pred) {
    if constexpr (std::bidirectional_iterator<Iterator>) {
        return std::prev(std::partition(std::next(first), last,
                                        [&pred, &first](auto &x)
                                            CPPLAB_ATTRIB_forceinline_lambda { return comp(x, *first); }));
    } else {
        Iterator i = first;
        for (Iterator j = ++first; j != last; ++j) {
            if (pred(*j, pivot)) {
                std::iter_swap(first, j);
                i = first;
                ++first;
            }
        }
        return i;
    }
}

template <typename Iterator, typename Predicate>
constexpr Iterator partition(Iterator first, Iterator last, Predicate pred) {
    auto pivot = std::move(*first);
    auto i = do_partition(first, last, pivot, pred);
    *first = std::move(*i);
    *i = std::move(pivot);
    return i;
}

} // namespace detail::qsort

template <typename Iterator, typename Compare = std::less<>>
constexpr void quick_sort(Iterator first, Iterator last, Compare comp = {}) {
    if (first == last)
        return;
    Iterator pivot = detail::qsort::partition(first, last, comp);
    quick_sort(first, pivot, comp);
    quick_sort(std::next(pivot), last, comp);
}

} // namespace cpplab

#endif

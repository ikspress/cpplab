#ifndef CPPLAB_MERGE_SORT_HH
#define CPPLAB_MERGE_SORT_HH

#include <algorithm>
#include <functional>
#include <iterator>

namespace cpplab {

template <typename Iterator, typename Compare = std::less<>>
void merge_sort(Iterator first, Iterator last, Compare comp = {}) {
    auto length = std::distance(first, last);
    if (length < 2)
        return;
    Iterator middle = std::next(first, length / 2);
    merge_sort(first, middle, comp);
    merge_sort(middle, last, comp);
    std::inplace_merge(first, middle, last, comp);
}

} // namespace cpplab

#endif

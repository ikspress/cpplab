#ifndef CPPLAB_SELECTION_SORT_HH
#define CPPLAB_SELECTION_SORT_HH

#include <algorithm>
#include <functional>

namespace cpplab {

template <typename Iterator, typename Compare = std::less<>>
constexpr void selection_sort(Iterator first, Iterator last, Compare comp = {}) {
    while (first != last) {
        std::iter_swap(first, std::min_element(first, last, comp));
        ++first;
    }
}

} // namespace cpplab

#endif

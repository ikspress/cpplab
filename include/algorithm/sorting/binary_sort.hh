#ifndef CPPLAB_BINARY_SORT_HH
#define CPPLAB_BINARY_SORT_HH

#include <algorithm>
#include <functional>
#include <iterator>
#include <utility>

namespace cpplab {

template <typename Iterator, typename Compare = std::less<>>
constexpr void binary_sort(Iterator first, Iterator last, Compare comp = {}) {
    if (first == last)
        return;
    for (auto i = std::next(first); i != last;) {
        auto next_i = std::next(i);
        if (comp(*i, *std::prev(i))) {
            auto temp = std::move(*i);
            auto j = std::upper_bound(first, i, temp, comp);
            std::move_backward(j, i, next_i);
            *j = std::move(temp);
        }
        i = next_i;
    }
}

} // namespace cpplab

#endif

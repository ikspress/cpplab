#ifndef CPPLAB_SORT_N_HH
#define CPPLAB_SORT_N_HH

#include "branchless.hh"
#include <algorithm>
#include <utility>

namespace cpplab::detail {

template <typename T, typename Compare>
constexpr void cond_swap(T &x, T &y, Compare comp) {
    bool r = comp(x, y);
    T temp = r ? x : y;
    y = r ? y : x;
    x = temp;
}

template <typename T, typename Compare>
constexpr void cond_swap_branched(T &x, T &y, Compare comp) {
    if (!comp(y, x))
        return;
    std::swap(x, y);
}

template <typename T, typename Compare>
constexpr void partially_sorted_swap(T &x, T &y, T &z, Compare comp) {
    bool r = comp(z, x);
    T temp = r ? z : x;
    z = r ? x : z;
    r = comp(temp, y);
    x = r ? x : y;
    y = r ? y : temp;
}

template <typename Iterator, typename Compare>
constexpr void sort3_branched(Iterator x1, Iterator x2, Iterator x3, Compare comp) {
    if (!comp(*x2, *x1)) {
        if (!comp(*x3, *x2))
            return;
        std::iter_swap(x2, x3);
        if (comp(*x2, *x1))
            std::iter_swap(x1, x2);
        return;
    }
    if (comp(*x3, *x2)) {
        std::iter_swap(x1, x3);
        return;
    }
    std::iter_swap(x1, x2);
    if (comp(*x3, *x2))
        std::iter_swap(x2, x3);
}

template <typename Iterator, typename Compare>
constexpr void sort3_branchless(Iterator x1, Iterator x2, Iterator x3, Compare comp) {
    auto a = std::move(*x1), b = std::move(*x2), c = std::move(*x3);
    cond_swap(b, c, comp);
    partially_sorted_swap(a, b, c, comp);
    *x1 = std::move(a);
    *x2 = std::move(b);
    *x3 = std::move(c);
}

template <typename Iterator, typename Compare>
constexpr void sort3(Iterator x1, Iterator x2, Iterator x3, Compare comp) {
    if constexpr (use_branchless_swap<Iterator, Compare>)
        sort3_branchless(x1, x2, x3, comp);
    else
        sort3_branched(x1, x2, x3, comp);
}

template <typename Iterator, typename Compare>
constexpr void sort4_branched(Iterator x1, Iterator x2, Iterator x3, Iterator x4, Compare comp) {
    sort3_branched(x1, x2, x3, comp);
    cond_swap_branched(*x3, *x4);
    cond_swap_branched(*x2, *x3);
    cond_swap_branched(*x1, *x2);
}

template <typename Iterator, typename Compare>
constexpr void sort4_branchless(Iterator x1, Iterator x2, Iterator x3, Iterator x4, Compare comp) {
    auto a = std::move(*x1), b = std::move(*x2), c = std::move(*x3), d = std::move(*x4);
    cond_swap(a, c, comp);
    cond_swap(b, d, comp);
    cond_swap(a, b, comp);
    cond_swap(c, d, comp);
    cond_swap(b, c, comp);
    *x1 = std::move(a);
    *x2 = std::move(b);
    *x3 = std::move(c);
    *x4 = std::move(d);
}

template <typename Iterator, typename Compare>
constexpr void sort4(Iterator x1, Iterator x2, Iterator x3, Iterator x4, Compare comp) {
    if constexpr (use_branchless_swap<Iterator, Compare>)
        sort4_branchless(x1, x2, x3, x4, comp);
    else
        sort4_branched(x1, x2, x3, x4, comp);
}

template <typename Iterator, typename Compare>
constexpr void sort5_branched(Iterator x1, Iterator x2, Iterator x3, Iterator x4, Iterator x5, Compare comp) {
    sort4_branched(x1, x2, x3, x4, comp);
    cond_swap_branched(*x4, *x5);
    cond_swap_branched(*x3, *x4);
    cond_swap_branched(*x2, *x3);
    cond_swap_branched(*x1, *x2);
}

template <typename Iterator, typename Compare>
constexpr void sort5_branchless(Iterator x1, Iterator x2, Iterator x3, Iterator x4, Iterator x5, Compare comp) {
    auto a = std::move(*x1), b = std::move(*x2), c = std::move(*x3), d = std::move(*x4), e = std::move(*x5);
    cond_swap(a, b, comp);
    cond_swap(d, e, comp);
    partially_sorted_swap(c, d, e, comp);
    cond_swap(b, e, comp);
    partially_sorted_swap(a, c, d, comp);
    partially_sorted_swap(b, c, d, comp);
    *x1 = std::move(a);
    *x2 = std::move(b);
    *x3 = std::move(c);
    *x4 = std::move(d);
    *x5 = std::move(e);
}

template <typename Iterator, typename Compare>
constexpr void sort5(Iterator x1, Iterator x2, Iterator x3, Iterator x4, Iterator x5, Compare comp) {
    if constexpr (use_branchless_swap<Iterator, Compare>)
        sort5_branchless(x1, x2, x3, x4, x5, comp);
    else
        sort5_branched(x1, x2, x3, x4, x5, comp);
}

} // namespace cpplab::detail

#endif

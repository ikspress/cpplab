#ifndef CPPLAB_USE_BRANCHLESS_PARTITION_HH
#define CPPLAB_USE_BRANCHLESS_PARTITION_HH

#include "is_simple_comparator.hh"
#include <iterator>
#include <type_traits>

namespace cpplab::detail {

template <typename Iterator, typename Compare>
constexpr inline auto use_branchless_partition =
    std::contiguous_iterator<Iterator> && std::is_scalar_v<std::iter_value_t<Iterator>>;

template <typename Iterator, typename Compare>
constexpr inline auto use_branchless_swap =
    is_simple_comparator<Compare> && std::is_scalar_v<std::iter_value_t<Iterator>>;

} // namespace cpplab::detail

#endif

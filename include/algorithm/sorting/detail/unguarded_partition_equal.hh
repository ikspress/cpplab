#ifndef CPPLAB_UNGUARDED_PARTITION_EQUAL_HH
#define CPPLAB_UNGUARDED_PARTITION_EQUAL_HH

#include <algorithm>

namespace cpplab::detail {

template <typename Iterator, typename Compare>
constexpr Iterator unguarded_partition_equal(Iterator first, Iterator last, Compare comp) {
    auto pivot = first;
    if (comp(*pivot, *--last)) {
        while (!comp(*pivot, *++first)) {}
        while (comp(*pivot, *--last)) {}
    } else {
        do
            if (++first == last)
                return ++first;
        while (!comp(*pivot, *first));
    }
    while (first < last) {
        std::iter_swap(first, last);
        while (!comp(*pivot, *++first)) {}
        while (comp(*pivot, *--last)) {}
    }
    return first;
}

} // namespace cpplab::detail

#endif

#ifndef CPPLAB_IS_SIMPLE_COMPARATOR_HH
#define CPPLAB_IS_SIMPLE_COMPARATOR_HH

#include <functional>
#include <type_traits>

namespace cpplab::detail {

template <typename T>
struct IsSimpleComparator : std::false_type {};
template <>
struct IsSimpleComparator<std::compare_three_way> : std::true_type {};
template <>
struct IsSimpleComparator<std::equal_to<>> : std::true_type {};
template <>
struct IsSimpleComparator<std::greater_equal<>> : std::true_type {};
template <>
struct IsSimpleComparator<std::greater<>> : std::true_type {};
template <>
struct IsSimpleComparator<std::less_equal<>> : std::true_type {};
template <>
struct IsSimpleComparator<std::less<>> : std::true_type {};
template <>
struct IsSimpleComparator<std::not_equal_to<>> : std::true_type {};
template <>
struct IsSimpleComparator<std::ranges::equal_to> : std::true_type {};
template <>
struct IsSimpleComparator<std::ranges::greater_equal> : std::true_type {};
template <>
struct IsSimpleComparator<std::ranges::greater> : std::true_type {};
template <>
struct IsSimpleComparator<std::ranges::less_equal> : std::true_type {};
template <>
struct IsSimpleComparator<std::ranges::less> : std::true_type {};
template <>
struct IsSimpleComparator<std::ranges::not_equal_to> : std::true_type {};
template <typename T>
struct IsSimpleComparator<std::equal_to<T>> : std::true_type {};
template <typename T>
struct IsSimpleComparator<std::greater_equal<T>> : std::true_type {};
template <typename T>
struct IsSimpleComparator<std::greater<T>> : std::true_type {};
template <typename T>
struct IsSimpleComparator<std::less_equal<T>> : std::true_type {};
template <typename T>
struct IsSimpleComparator<std::less<T>> : std::true_type {};
template <typename T>
struct IsSimpleComparator<std::not_equal_to<T>> : std::true_type {};

template <typename Iterator>
constexpr inline auto is_simple_comparator = IsSimpleComparator<Iterator>::value;

} // namespace cpplab::detail

#endif

#ifndef CPPLAB_UNGUARDED_INSERTION_SORT_HH
#define CPPLAB_UNGUARDED_INSERTION_SORT_HH

#include <iterator>
#include <utility>

namespace cpplab::detail {

template <typename Iterator, typename Compare>
constexpr void unguarded_insertion_sort(Iterator first, Iterator last, Compare comp) {
    while (first != last) {
        if (auto i = std::prev(first); comp(*first, *i)) {
            auto j = first;
            auto temp = std::move(*j);
            do {
                *j = std::move(*i);
                j = i;
            } while (comp(temp, *--i));
            *j = std::move(temp);
        }
        ++first;
    }
}

} // namespace cpplab::detail

#endif

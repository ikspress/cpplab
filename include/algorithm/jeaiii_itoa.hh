#ifndef CPPLAB_JEAIII_ITOA_HH
#define CPPLAB_JEAIII_ITOA_HH

#include <concepts>
#include <expected>
#include <system_error>
#include <type_traits>

namespace cpplab::jeaiii {

// https://github.com/jeaiii/itoa
template <std::integral T>
    requires(!std::is_same_v<T, bool>)
constexpr std::expected<char *, std::errc> to_chars(char *first, char *last, T value) noexcept {}

} // namespace cpplab::jeaiii

#endif
